Introduction: Vault admin must manage multiple vault environments and vault resources i.e. namespaces(tenancies), client-policies, secrets engines, authorization methods etc.

Objective: Managing the entire infrastructure of Vault using infrastructure as a code, that helps us to provide great user experience along with other benefits.

Solution: 
Terraform
Ansible
Or Both

Option 1: Terraform (POC)

“ Hashicorp Terraform is an infrastructure as code which enables the operation team to codify the Vault configuration tasks such as the creation of policies. Automation through codification allows operators to increase their productivity more quicker, promote repeatable process, and reduce human error.” - Hashicorp official website

Assumption: Vault and Terraform both are the products of Hashicorp. So, they naturally fit together.

Implementation: a minimal version of the concept has been implemented in following bitBucket repo:

Preview of BitBucket Repository:
Repo will automate vault configuration and Terraform will be used to provision following resources.

Tools:
Terraform (open source)
Terraform vault provider (open source)

Resources
Description
Namespaces
Namespaces.tf will create “GAIA-TEST” and “GAIA-DEV” namespaces
Policies
Policies.tf will create ‘ad’ policy at Root namespace and ‘GAIA-TEST’ namespace.
Secrets engine
Secrets.tf will enable ‘kv-tf’ secrets engine of type ‘kv-v2’ at Root namespace and ‘GAIA-TEST’ namespace.







